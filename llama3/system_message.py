import datetime
# This file contains the system messages that the user will receive when they are prompted to enter information.

def system_message_category(category) : 
    with open('llama3/message/message_category.txt', 'r') as fichier:
    # Lire le contenu du fichier
        contenu = fichier.read()
    message =  "you chose only one of the following categories: "+", " .join(category) + contenu
    return message


def system_message_resume():
    with open('llama3/message/message_summary.txt', 'r') as fichier:
    # Lire le contenu du fichier
        contenu = fichier.read()
    return contenu


system_message_calendar = (f"""
    today is   {datetime.datetime.now()} the time zone is Europe/Paris
    You are an email assistant. You have to create an event in the calendar in JSON format.
    It's important to respect the json format, and don't write anything else.
    This is an example of the format you will use:
{{
        "summary": "title",
        "location": "location",
        "description": "description", 
        "start": {{
            "dateTime": "YYYY-MM-DDTHH:MM:SS",
            "timeZone": "time zone (e.g., Europe/Paris)"
        }},
        "end": {{
            "dateTime": "YYYY-MM-DDTHH:MM:SS",
            "timeZone": "time zone"
        }},
        "reminders": {{
            "useDefault": "true or false"
        }}
    }}
""")


def system_message_not_accept_appointment():
    return (f"""
    i have an appointment with this email, you write a json texte that i dont't accepte the appointment.
    the format of the date is YYYY-MM-DD and the time is HH:MM .
    It's important to respect the json format, and don't write anything else.
    Don't change the format of body, just write only the information.
    
    This is an example of the format you will use,don't write anything else don't change the json format:
    {{
        "to": "email address",
        "subject": "title of the email",
        "body": "<!DOCTYPE html>\\n<html lang=\\"en\\">\n<head>\\n<meta charset= \\"UTF-8\\">\\n<meta name=\\"viewport\\" content=\\"width=device-width, initial-scale=1.0\\">\\n<title>Notification</title>\\n<style>\\nbody {{\\nfont-family: Arial, sans-serif;\\ndisplay: flex;\\njustify-content: center;\\nalign-items: center;\\nheight: 100vh;\\nmargin: 0;\\nbackground-color: #f4f4f9;\\ncolor: #333;\\n}}\\n.container {{\\ntext-align: center;\\npadding: 40px;\\nmax-width: 500px;\\nbackground-color: #fff;\\nbox-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);\\nborder-radius: 8px;\\n}}\\n.container h1 {{\\nfont-size: 24px;\\nmargin-bottom: 16px;\\ncolor: #ff6f61;\\n}}\\n.container p{{\\nfont-size: 16px;\\nline-height: 1.6;\\ncolor: #555;\\n}}\\n.container a {{\\ndisplay: inline-block;\\nmargin-top: 20px;\\npadding: 10px 20px;\\ncolor: #fff;\\nbackground-color: #ff6f61;\\ntext-decoration: none;\\nborder-radius: 5px;\\ntransition: background-color 0.3s;\\n}}\\n.container a:hover {{\\nbackground-color: #e65c4f;\\n}}\\n</style>\\n</head>\\n<body>\\n<div class=\\"container\\">\\n<h1>[title of appointment]</h1>\\n<p>Hello,</p>\\n<p>\\n[the information about the appointment]\\n</p>\\n<p>I am available to reschedule the appointment at a later date.</p>\\n<a href=\\"[my email]\\">Contact Me</a>\\n</div>\\n</body>\\n</html>"

    }}
""")

def system_message_new_accept_appointment(date):
    return (f"""
    i have an appointment with this email, you write a json texte that i reschedule the appointment and i'm available in this date {date} 
    the format of the date is YYYY-MM-DD and the time is HH:MM .
    It's important to respect the json format, and don't write anything else.
    Don't change the format of body, just write only the information.
    
    This is an example of the format you will use,don't write anything else don't change the json format:
    {{
        "to": "email address",
        "subject": "title of the email",
        "body": "<!DOCTYPE html>\\n<html lang=\\"en\\">\n<head>\\n<meta charset= \\"UTF-8\\">\\n<meta name=\\"viewport\\" content=\\"width=device-width, initial-scale=1.0\\">\\n<title>Notification</title>\\n<style>\\nbody {{\\nfont-family: Arial, sans-serif;\\ndisplay: flex;\\njustify-content: center;\\nalign-items: center;\\nheight: 100vh;\\nmargin: 0;\\nbackground-color: #f4f4f9;\\ncolor: #333;\\n}}\\n.container {{\\ntext-align: center;\\npadding: 40px;\\nmax-width: 500px;\\nbackground-color: #fff;\\nbox-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);\\nborder-radius: 8px;\\n}}\\n.container h1 {{\\nfont-size: 24px;\\nmargin-bottom: 16px;\\ncolor: #ff6f61;\\n}}\\n.container p{{\\nfont-size: 16px;\\nline-height: 1.6;\\ncolor: #555;\\n}}\\n.container a {{\\ndisplay: inline-block;\\nmargin-top: 20px;\\npadding: 10px 20px;\\ncolor: #fff;\\nbackground-color: #ff6f61;\\ntext-decoration: none;\\nborder-radius: 5px;\\ntransition: background-color 0.3s;\\n}}\\n.container a:hover {{\\nbackground-color: #e65c4f;\\n}}\\n</style>\\n</head>\\n<body>\\n<div class=\\"container\\">\\n<h1>[title of appointment]</h1>\\n<p>Hello,</p>\\n<p>\\n[the information about the appointment]\\n</p>\\n<p>I am available to reschedule the appointment at a later date.</p>\\n<a href=\\"[my email]\\">Contact Me</a>\\n</div>\\n</body>\\n</html>"

    }}
""")

def system_message_accept_appointment(): 
    return (f"""
    i have an appointment with this email, you write a json texte that i confirm the appointment. 
    the format of the date is YYYY-MM-DD and the time is HH:MM .
    It's important to respect the json format, and don't write anything else.
    Don't change the format of body, just write only the information.
    
    This is an example of the format you will use,don't write anything else don't change the json format:
    {{
        "to": "email address",
        "subject": "title of the email",
        "body": "<!DOCTYPE html>\\n<html lang=\\"en\\">\n<head>\\n<meta charset= \\"UTF-8\\">\\n<meta name=\\"viewport\\" content=\\"width=device-width, initial-scale=1.0\\">\\n<title>Notification</title>\\n<style>\\nbody {{\\nfont-family: Arial, sans-serif;\\ndisplay: flex;\\njustify-content: center;\\nalign-items: center;\\nheight: 100vh;\\nmargin: 0;\\nbackground-color: #f4f4f9;\\ncolor: #333;\\n}}\\n.container {{\\ntext-align: center;\\npadding: 40px;\\nmax-width: 500px;\\nbackground-color: #fff;\\nbox-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);\\nborder-radius: 8px;\\n}}\\n.container h1 {{\\nfont-size: 24px;\\nmargin-bottom: 16px;\\ncolor: #ff6f61;\\n}}\\n.container p{{\\nfont-size: 16px;\\nline-height: 1.6;\\ncolor: #555;\\n}}\\n.container a {{\\ndisplay: inline-block;\\nmargin-top: 20px;\\npadding: 10px 20px;\\ncolor: #fff;\\nbackground-color: #ff6f61;\\ntext-decoration: none;\\nborder-radius: 5px;\\ntransition: background-color 0.3s;\\n}}\\n.container a:hover {{\\nbackground-color: #e65c4f;\\n}}\\n</style>\\n</head>\\n<body>\\n<div class=\\"container\\">\\n<h1>[title of appointment]</h1>\\n<p>Hello,</p>\\n<p>\\n[the information about the appointment]\\n</p>\\n<p>I am available to reschedule the appointment at a later date.</p>\\n<a href=\\"[my email]\\">Contact Me</a>\\n</div>\\n</body>\\n</html>"
    }}
""")





def main ():
    print (system_message_category(["appointment", "meeting", "reminder"]))

if __name__ == "__main__":
    main()