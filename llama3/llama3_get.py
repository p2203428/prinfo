import openai
import json
import sys
sys.path.append('llama3')
from system_message import *

#create a chat completion
def get_message(system_message,text):
    return openai.ChatCompletion.create(
        model = "local-model",
        messages = [
            {"role": "system", "content": system_message},
            {"role": "user", "content": text}
        ],
        temperature = 0.7,
    )


def get_html_resum(important,appointment,nb_email):
    return get_message(system_message_resume(),"This is the important email" + str(important) + "this is the appointment email"+ str(appointment) + "this is the number of email" + str(nb_email)).choices[0].message.content

def get_category(categorys, text):
    json_object = None
    while json_object is None:
        try:
            json_object = json.loads(get_message(system_message_category(categorys),str(text)).choices[0].message.content)
            
            return json_object
        except ValueError as e:
            json_object = None

def get_is_an_appointment(text):
    response = get_message(system_message_is_an_appointment, str(text)).choices[0].message.content.strip().lower()
    return response == "true"


def get_appointment(text):
    json_object = None
    while json_object is None:
        try:
            json_object = json.loads(get_message(system_message_calendar, text).choices[0].message.content)
            
            return json_object
        except ValueError as e:
            json_object = None
    
def get_accept_appointment(email):
    json_object = None
    while json_object is None:
        try:
            json_object = json.loads(get_message(str(system_message_accept_appointment()), str(email)).choices[0].message.content)
            
            return json_object
        except ValueError as e:
            json_object = None      

def get_new_accepted_appointment(email,date):
    json_object = get_message(str(system_message_new_accept_appointment(date)), str(email)).choices[0].message.content
    print(str(json_object))
    #while json_object is None:
     #   try:
    json_object = json.loads(json_object)
         
    return json_object
        #except ValueError as e:
         #   json_object = None

def get_not_accepted_appointment(email):
    json_object = get_message(str(system_message_not_accept_appointment()), str(email)).choices[0].message.content
    print(str(json_object))
    #while json_object is None:
     #   try:
    json_object = json.loads(json_object)
         
    return json_object
        #except ValueError as e:
         #   json_object = None

def get_title(text):
    t= get_message(system_message_title,text).choices[0].message.content
    while len(t) > 30:
        t = get_message(system_message_title,text).choices[0].message.content
    return get_message(system_message_title,text).choices[0].message.content

def get_date(text):
    d = get_message(system_message_date, text).choices[0].message.content
    while d != "%Y-%m-%d":
        d = get_message(system_message_date, text).choices[0].message.content
    return d

def get_time(text):
    t = get_message(system_message_time,text).choices[0].message.content
    while t != "%H:%M":
        t = get_message(system_message_time,text).choices[0].message.content
    return t

def get_location(text):
    return get_message(system_message_location,text).choices[0].message.content