# prinfo: Assistant Virtuel avec LLM

## Contexte du projet

Ce projet fait partie de l'UE LIFprojet, il s'agit du sujet **RC4-A : Large Language Models (LLM) pour un assistant virtuel**.

[Lien pour la présentation](https://www.canva.com/design/DAGYa8CJ46Q/Zma3W53_ypH9PBYL4Si17A/view?utm_content=DAGYa8CJ46Q&utm_campaign=designshare&utm_medium=link2&utm_source=uniquelinks&utlId=hfd2ff7cc49)

# Objectifs principaux :

L'objectif du projet est de créer un assistant capable de classer automatiquement des messages dans la catégories correspondantes, de répondre automatiquement ou de proposer des réponses adaptées aux messages reçus, et de gérer les demandes de rendez-vous en détectant celles présentes dans les messages et en les ajoutant automatiquement dans un agenda. L'interaction avec l'utilisateur se fait aussi via des questions à choix multiples pour prendre certaines décisions, garantissant une prise de décision précise et adaptée aux différents contextes.

## Installation
### Librairies à installer:
  - pip install google-auth, gère l'authentification avec l'API Google
  - pip install httplib2, nécessaire pour gérer les requêtes HTTP avec l'API Google
  - pip install google-api-python-client
  - pip install beautifulsoup4
  - pip install flask
  - pip install openai
  - pip install requests
  - pip install oauth2client
  - pip install google-auth-oauthlib
  - pip install requests



# Organization du code
# ~/prinfo le nom du projet

### Organization du code
**Le dossier api**
- *calendar/calendar_event.py*
  - Ce fichier contient des fonctions qui permet de créer un évènement (rendez-vous ou anniversaire par exemple) via l'API Google Calendar mais aussi d'en supprimer un.
- *credentials/credentials.json*
  - C'est un fichier de configuration pour nous permettre  d'accéder aux services Google (Google Calendar, Google Drive et Gmail) .
- *gmail/api_test.py*
  - Le fichier permet d'extraire les détails d'un email récent (sujet, corps, ...) via l'API Gmail. Ensuite il efface tout son contenu HTML qui n'est pas nécessaire.
- *gmail/function.py*
  - Ce fichier est un outil complet pour la gestion des emails dans Gmail via l'API, offrant des fonctionnalités d'envoi, de catégorisation par labels (rendez-vous, spam, ...), de récupération d'ID du mail et de nettoyage du contenu en supprimant le code html.


**Le dossier flask**
- *init.py*


**prinfo/api/calendar_event.py**
- Ce fichier contient des fonctions qui permettent de créer un événement (rdv par exemple) via l'API Google Calendar mais aussi de supprimer un.
**prinfo/api/calendar_event.py**
- Ce fichier contient des fonctions qui permettent de créer un événement (rdv par exemple) via l'API Google Calendar mais aussi de supprimer un.

**prinfo/api/credentials/credentials.json**
- C'est un fichier de configuration pour nous permettre d'accéder aux services Google (Google Calendar, Google Drive, Gmail).
**prinfo/api/credentials/credentials.json**
- C'est un fichier de configuration pour nous permettre d'accéder aux services Google (Google Calendar, Google Drive, Gmail).

**prinfo/api/gmail/api_test.py**
- Le fichier permet d'extraire les détails d'un email récent (sujet, corps..) via l'API Gmail. Ensuite, il efface tout son contenu HTML qui n'est pas nécessaire.
**prinfo/api/gmail/api_test.py**
- Le fichier permet d'extraire les détails d'un email récent (sujet, corps..) via l'API Gmail. Ensuite, il efface tout son contenu HTML qui n'est pas nécessaire.

**prinfo/api/gmail/function.py**
- Ce fichier est un outil complet pour la gestion des emails dans Gmail via l'API, offrant des fonctionnalités d'envoi, de catégorisation par labels (rdv, spam...), de récupération d'un id du mail et de nettoyage du contenu en supprimant du code HTML.
**prinfo/api/gmail/function.py**
- Ce fichier est un outil complet pour la gestion des emails dans Gmail via l'API, offrant des fonctionnalités d'envoi, de catégorisation par labels (rdv, spam...), de récupération d'un id du mail et de nettoyage du contenu en supprimant du code HTML.

- # Le dossier flask

**flask/__init__.py**
- Ce fichier exécute un programme Flask pour analyser les emails et, en cas de détection de rendez-vous, crée un événement dans Google Calendar. Puis, il envoie un email de confirmation d'acceptation du rendez-vous.
**flask/__init__.py**
- Ce fichier exécute un programme Flask pour analyser les emails et, en cas de détection de rendez-vous, crée un événement dans Google Calendar. Puis, il envoie un email de confirmation d'acceptation du rendez-vous.

- # le dossier llama3

**llama3/llama3_get.py**
- Les fonctions de ce fichier permettent d'analyser les emails et extraire automatiquement des informations pour la gestion des rendez-vous en interagissant avec un modèle de langage qui détecte si c'est un rendez-vous ou pas.
**llama3/llama3_get.py**
- Les fonctions de ce fichier permettent d'analyser les emails et extraire automatiquement des informations pour la gestion des rendez-vous en interagissant avec un modèle de langage qui détecte si c'est un rendez-vous ou pas.

**llama3/llama3_system.py**
- Ce fichier vérifie essentiellement si l'utilisateur pourra prendre un rendez-vous sur un créneau qu'il a choisi. Sinon, il doit choisir un autre créneau.
**llama3/llama3_system.py**
- Ce fichier vérifie essentiellement si l'utilisateur pourra prendre un rendez-vous sur un créneau qu'il a choisi. Sinon, il doit choisir un autre créneau.

**llama3/system_message.py**
- La plupart de ce fichier définit des messages et des instructions qui seront utilisés par un assistant virtuel ou par un modèle de langage (LLM) pour traiter des mails liés à des rendez-vous. Les instructions guident l'utilisateur pour saisir les bonnes données quand il est prompté de les entrer.
**llama3/system_message.py**
- La plupart de ce fichier définit des messages et des instructions qui seront utilisés par un assistant virtuel ou par un modèle de langage (LLM) pour traiter des mails liés à des rendez-vous. Les instructions guident l'utilisateur pour saisir les bonnes données quand il est prompté de les entrer.

## Prérequis

- **Python 3.x** : Assurez-vous qu'une version récente de Python est installée.
- **Environnement virtuel** : Il est recommandé d'utiliser un environnement virtuel pour installer les dépendances.

### Langages

- Langage principal: Python
- LLM: Language informatique d'intelligence artificielle capable d'interpréter le langage humain suite à une requête. Puis, nous donner une réponse.

**Le dossier llama3**
- *llama3_get.py*
- *llama3_system.py*
- *system_message.py*

**Le dossier SQL**
- *test.py*



### Prérequis
- **Python 3.x**: Assurez-vous qu'une version recente de python est installé.
- **Environnement virtuel**: Il est recommandé d'utiliser un environnement virtuel pour installer les dépendances.




### Langages et librairies
- **Language principale**: Python
- **LLM**: Language informatique d'intelligence artificielle capable d'interpréter le language humain suite a une requête, puis nous donner une reponse.
- **Librairies proposées** :
  - [LM Studio](https://lmstudio.ai/): Pour exécuter un modèle de langage localement.
  - [LangChain](https://www.langchain.com/): Pour gérer les chaînes de traitement des messages.
  - [Haystack](https://haystack.deepset.ai/): Pour des recherches et interactions basées sur le langage naturel.


### Collaborateurs
  - Samir MOHAMED HAFFOUDHI p2106997
  - Mona SHAWKI p2203428
  - Ali CHABAKA p2200473





