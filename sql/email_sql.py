import sqlite3


# Connexion ou création de la base de données SQLite
conn = sqlite3.connect('sql/data/emails.db')  #Creation du fichier email.db si existe pas 


cursor = conn.cursor()



def insert_email(id_mail, date_mail, category_mail, corps_mail, subject_mail, mail, important):
    insert_query = f"""
    INSERT INTO email (id_mail, date_mail, category_mail, corps_mail, subject_mail, mail, important) 
    VALUES (?, ?, ?, ?, ?, ?, ?)
    """
    
    try:
        
        
        # Exécuter la requête
        cursor.execute(insert_query, (id_mail, date_mail, category_mail, corps_mail, subject_mail, mail, important))
        
        # Valider les changements
        conn.commit()
        print("Données insérées avec succès dans la table 'email'.")
    
    except sqlite3.Error as e:
        # Gestion des erreurs liées à la clé étrangère ou autres
        print(f"Erreur lors de l'insertion dans 'email': {e}")



def insert_appointment(id_mail, email, titre, lieu, date_start, date_fin, accepter, resume):
    
    
    # Construire la requête SQL
    insert_query = f"""
    INSERT INTO appointment (id_mail, email, titre, lieu, date_start, date_fin, accepter, resume)
    VALUES (?, ?, ?, ?, ?, ?, ?,?)
    """
    
    try:
        # Exécuter la requête
        cursor.execute(insert_query, (id_mail, email, titre, lieu, date_start, date_fin, accepter, resume))
        
        # Valider les changements
        conn.commit()
        print("Données insérées avec succès dans la table 'appointment'.")
    
    except sqlite3.Error as e:
        # Gestion des erreurs liées à la clé étrangère ou autres
        print(f"Erreur lors de l'insertion dans 'appointment': {e}")


def insert_spam(mail,why):
    insert_query=f"""
    INSERT INTO spam (mail,why)
    VALUES (?, ?)
    """

    try:
        cursor.execute(insert_query, (mail,why))
        conn.commit()
        print("les donneé de la table spam inseree avec succes ")
    except sqlite3.Error as e:
        # Gestion des erreurs liées à la clé étrangère ou autres
        print(f"Erreur lors de l'insertion dans 'appointment': {e}")



def insert_important(id_mail,email,resume):
    insert_query=f"""
    INSERT INTO important(id_mail,email,resume)
    VALUES (?, ?, ?)
    """

    try:
        cursor.execute(insert_query, (id_mail, email,resume))
        conn.commit()
        print("les donnee de la table important sont bien inseree")
    except sqlite3.Error as e:
            print(f"Erreur lors de l'insertion dans 'important': {e}")





def get_email_by_id(id_mail):
    
    try:
        
        cursor.execute(f"SELECT * FROM email WHERE id_mail = '{id_mail}'")
        
        # Récupérer le résultat (le tuple de l'email)
        email = cursor.fetchone()
        
        # Vérifier si un email a été trouvé
        if email:
            print("Email trouvé:", email)
        else:
            print(f"Aucun email trouvé avec l'id: {id_mail}")
    
    except sqlite3.Error as e:
        print(f"Erreur lors de la récupération de l'email: {e}")



def get_all_rows(table_name):
    
    try:
        
        query = f"SELECT * FROM {table_name}"
        
        # Exécuter la requête
        cursor.execute(query)
        
        # Récupérer tous les résultats
        rows = cursor.fetchall()
        return rows
    
    except sqlite3.Error as e:
        # Gestion des erreurs SQL
        print(f"Erreur lors de la récupération des données de la table '{table_name}': {e}")
        return []


def reset_autoincrement(table_name):#cette fonction supprime tous les donnee d'une table et itialise sa cle primaire
    
    
    try:
       
        cursor.execute(f"DELETE FROM {table_name}")
        cursor.execute(f"DELETE FROM sqlite_sequence WHERE name='{table_name}'")
        conn.commit()
        print(f"Le compteur AUTOINCREMENT de la table '{table_name}' a été réinitialisé.")
    except sqlite3.Error as e:
        # Gestion des erreurs
        print(f"Erreur lors de la réinitialisation de l'AUTOINCREMENT pour '{table_name}': {e}")




def drop_all_tables():

    tables = ['email', 'appointment', 'spam', 'important']
    
    try:
        for table in tables:
            drop_query = f"DROP TABLE IF EXISTS {table}"
            cursor.execute(drop_query)
            print(f"La table '{table}' a été supprimée avec succès.")
            conn.commit()
    except sqlite3.Error as e:
        print(f"Erreur lors de la suppression des tables : {e}")






def create_all_tables():
    
    # Activer les clés étrangères
    conn.execute("PRAGMA foreign_keys = 1")

    try:
        # Création de la table 'email'
        email_table_query = """
        CREATE TABLE IF NOT EXISTS email (
            id_mail TEXT PRIMARY KEY,
            date_mail TEXT ,
            category_mail TEXT,
            corps_mail TEXT ,
            subject_mail TEXT,
            mail TEXT ,
            important BOOLEAN  CHECK (important IN (0, 1))
        )
        """
        cursor.execute(email_table_query)

        # Création de la table 'appointment'
        appointment_table_query = """
        CREATE TABLE IF NOT EXISTS appointment (
            appointment_id INTEGER PRIMARY KEY AUTOINCREMENT,
            id_mail TEXT,
            email TEXT ,
            titre TEXT ,
            lieu TEXT,
            date_start TEXT ,
            date_fin TEXT ,
            accepter TEXT CHECK (accepter IN ('accepted', 'resend', 'canceled')),
            resume TEXT, 
            FOREIGN KEY (id_mail) REFERENCES email(id_mail) ON DELETE CASCADE
        )
        """
        cursor.execute(appointment_table_query)

        # Création de la table 'spam'
        spam_table_query = """
        CREATE TABLE IF NOT EXISTS spam (
            spam_id INTEGER PRIMARY KEY AUTOINCREMENT,
            mail TEXT ,
            why TEXT 
        )
        """
        cursor.execute(spam_table_query)

        # Création de la table 'important'
        important_table_query = """
        CREATE TABLE IF NOT EXISTS important (
            important_id INTEGER PRIMARY KEY AUTOINCREMENT,
            id_mail TEXT,
            email TEXT ,
            resume TEXT,
            FOREIGN KEY (id_mail) REFERENCES email(id_mail) ON DELETE CASCADE
        )
        """
        cursor.execute(important_table_query)

        

        # Validation des changements
        conn.commit()
        print("Toutes les tables ont été créées avec succès.")

    except sqlite3.Error as e:
        print(f"Erreur lors de la création des tables: {e}")


def fetch_data_from_sql():
    

    # Récupérez les emails importants
    cursor.execute("SELECT id_mail, email, resume FROM important")
    important = [{"id_mail": row[0], "email": row[1], "resume": row[2]} for row in cursor.fetchall()]

    # Récupérez les rendez-vous
    cursor.execute("SELECT id_mail, email, accepter, resume FROM appointment")
    appointment = [{"id_mail": row[0], "email": row[1], "type": row[2], "resume": row[3]} for row in cursor.fetchall()]

    # Comptez les emails
    cursor.execute("SELECT COUNT(*) FROM email")
    total_emails = cursor.fetchone()[0]

    
    return important, appointment, total_emails   












#insert_spam("voici un mail","c est pour cela il est important ")


#essayer la fonction
#insert_appointment("id3","cela est un mail","Rdv ","43 rue z","18h00","19h00",0)
#insert_new_appointment(3,"c le mail","Titre de votre mail","rue z","8h00","9h00")
#insert_important("corps du mail","c le resume du mail")
#get_all_rows("email")
#insert_email('id4', '2024-11-17', 'sortir', ' corps du mail.', 'Rappel ', 'lailaarnaud04@gmail.com', 0)
#get_all_rows("new_appointment")
#get_all_rows("spam")
#get_all_rows("email")


#reset_autoincrement("new_appointment")



#cursor.close()
#conn.close()
#print("Connexion fermée.")


if __name__ == '__main__':
    
    print(get_all_rows("appointment"))
    #print(get_all_rows("important"))
    #print(fetch_data_from_sql)