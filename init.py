from jinja2 import Environment, FileSystemLoader
import keyboard
import threading
import time
import sys
sys.path.append('api')
from authenticate import *
sys.path.append('api/gmail')
from gmail import *
sys.path.append('api/calendar')
from calendar_event import *
sys.path.append('llama3')
from llama3_system import *
sys.path.append('sql')
from email_sql import *


def run_api_test():
    
    messages = get_email(1)
    if not messages:
        print('no email found.')
    else:
            
        print('list email:')
        for message in messages:
            email_details = get_email_details(message['id'])
            print(f"message['id'] : {message['id']}")
            print(f"From: {email_details['From']}")
            print(f"Date: {email_details['Date']}")
            print(f"Subject: {email_details['Subject']}")
            print(f"Body:\n{email_details['Body']}")
            
            

            categorys = tous_les_label()
            category = get_category(categorys,email_details)
            print(category['Category'])
            insert_email(message['id'], str(email_details['Date']), str(category['Category']), str(email_details['Body']), str(email_details['Subject']), str(email_details['From']), str(category['Important']))
            
            if (category['Appointment']):
                appointement = get_appointment(str(email_details))
                
                if(input("vous avez un rendez-vous acceptez-vous ? (Y/n) ") == "Y"):
                    print(appointement['summary'])
                    create_event(appointement)
                    accept_appointement = get_accept_appointment(email_details)
                    send_email(email_details['From'], accept_appointement['subject'], accept_appointement['body'])
                    insert_appointment(str(message['id']),str(email_details['From']), str(appointement['summary']), str(appointement['location']), str(appointement['start']['dateTime']), str(appointement['end']['dateTime']), "accepted", str(category['Summarizing']))
                elif (input("vous voulez une autre date? (Y,n) ") == "Y"):
                    date = input("donner une date en format YYYY-MM-DD ")
                    heure = input("donner une heure en format HH:MM ")
                    new_date = f"{date} {heure}"
                    new_accept_appointement = get_new_accepted_appointment(email_details, new_date)
                    send_email(email_details['From'], new_accept_appointement['subject'], new_accept_appointement['body'])
                    insert_appointment(message['id'],email_details['From'], appointement['summary'], appointement['location'], appointement['start']['dateTime'], appointement['end']['dateTime'], "resend", str(category['Summarizing']))
                else:
                    insert_appointment(message['id'],email_details['From'], appointement['summary'], appointement['location'], appointement['start']['dateTime'], appointement['end']['dateTime'], "canceled", str(category['Summarizing']))
                    not_accept_appointement = get_not_accepted_appointment(email_details)
                    send_email(email_details['From'], not_accept_appointement['subject'], not_accept_appointement['body'])
            if (category['Important'] and not category['Appointment']):
                insert_important(message['id'],email_details['From'], category['Summarizing'])


            apply_label_to_email(str(message['id']), str(category['Category']))
            
            print("="*50)
            




def run_resume():
    print('summary email start:')

    # 1. Récupérer les données depuis SQL
    important, appointments, total_emails = fetch_data_from_sql()

    # 2. Charger le modèle HTML
    env = Environment(loader=FileSystemLoader('templates'))  # Assurez-vous que le dossier contient html_summary.html
    template = env.get_template('html_summary.html')

    # 3. Rendre le modèle avec les données
    render = template.render(important=important, appointments=appointments, total_emails=total_emails)

    # 4. Envoyer l'email
    send_email('lailaarnaud04@gmail.com', 'Resume', str(render))
    print('summary email sent.')

         

if __name__ == '__main__':
    drop_all_tables()
    create_all_tables()
    while True:
        if keyboard.is_pressed('q'):
            run_resume()
            print('You ended the program')
            break
        run_api_test()
        time.sleep(5)
    
