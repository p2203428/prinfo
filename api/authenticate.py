import os.path
import base64
from email.mime.text import MIMEText
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.discovery import build

# SCOPES pour envoyer des emails et modifier la boîte de réception
SCOPES = [
    'https://www.googleapis.com/auth/gmail.send',
    'https://www.googleapis.com/auth/gmail.labels',
    'https://www.googleapis.com/auth/gmail.modify',
    'https://www.googleapis.com/auth/calendar'
]

def authenticate_gmail_api(type):
    """Authentifie l'utilisateur via OAuth2 et retourne le service Gmail."""
    creds = None
    if os.path.exists('api/token/token.json'):
        creds = Credentials.from_authorized_user_file('api/token/token.json', SCOPES)
    
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('api/credentials/credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)

        with open('api/token/token.json', 'w') as token:
            token.write(creds.to_json())

    if type == "gmail":
        service = build('gmail', 'v1', credentials=creds)
    elif type == "calendar":
        service = build('calendar', 'v3', credentials=creds)
    return service

