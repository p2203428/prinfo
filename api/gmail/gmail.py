import re
import base64
from email.mime.multipart import MIMEMultipart
import sys
sys.path.append('api')
from authenticate import *
from bs4 import BeautifulSoup

def create_message(destinataire, subject, corps_mail):
    """Crée un message MIME pour l'API Gmail."""
    message = MIMEMultipart("Alternative")
    message['to'] = destinataire
    message['subject'] = subject
    message.attach(MIMEText(corps_mail, "html"))
    raw_message = base64.urlsafe_b64encode(message.as_bytes()).decode('utf-8')
    return {'raw': raw_message}

def send_email(destinataire, subject, corps_mail):
    """Envoie un email uniquement, sans gérer les labels."""
    try:
        service = authenticate_gmail_api("gmail")
        message = create_message(destinataire, subject, corps_mail)
        sent_message = service.users().messages().send(userId='me', body=message).execute()
        print(f"Email envoyé avec succès, ID du message : {sent_message['id']}")
        return sent_message['id']
    
    except Exception as e:
        print(f"Erreur lors de l'envoi de l'email : {e}")
        return None 

def get_label_id(label_name):
    """Récupère l'ID d'un label donné. Le crée si nécessaire."""
    try:
        results = authenticate_gmail_api("gmail").users().labels().list(userId='me').execute()
        labels = results.get('labels', [])
        for label in labels:
            if label['name'].lower() == label_name.lower():
                return label['id']

        return create_label(label_name)
    
    except Exception as e:
        print(f"Erreur lors de la récupération du libellé : {e}")
        return None

def create_label(label_name):
    """Crée un nouveau libellé dans Gmail si n'existe pas déjà."""
    try:
        label = {
            'labelListVisibility': 'labelShow',
            'messageListVisibility': 'show',
            'name': label_name
        }
        created_label = authenticate_gmail_api("gmail").users().labels().create(userId='me', body=label).execute()
        print(f"Libellé '{label_name}' créé avec succès. ID : {created_label['id']}")
        return created_label['id']
    
    except Exception as e:
        # Si le label existe déjà, on renvoie None
        if "Label name exists or conflicts" in str(e):
            print(f"Le libellé '{label_name}' existe déjà.")
            return get_label_id(authenticate_gmail_api("gmail"), label_name)
        else:
            print(f"Erreur lors de la création du libellé : {e}")
            return None
        



def apply_label_to_email(id_mail, label_name):
    """Applique le label label_name au email id_mail."""
    try:
        if id_mail:
            # Récupérer l'ID du label_name
            label_id = get_label_id(label_name)
            
            if label_id:
                # Appliquer le label au mail
                modify_message_with_label(id_mail, label_id)
                # suprimmer l'email de la boite de reception
                authenticate_gmail_api("gmail").users().messages().modify( userId='me', id=id_mail, body={'removeLabelIds': ['INBOX']}).execute()
            else:
                print("erreur à la creation du label.")
        else:
            print("Aucun email à étiqueter.")
    
    except Exception as e:
        print(f"Erreur lors de l'application du libellé : {e}")



def modify_message_with_label( message_id, label_id):
    """Modifie un message pour lui ajouter un label."""
    try:
        msg_labels = {'addLabelIds': [label_id]}
        modified_message = authenticate_gmail_api("gmail").users().messages().modify(userId='me', id=message_id, body=msg_labels).execute()
        print(f"Le message ID: {message_id} a été déplacé dans le libellé avec succès.")
    
    except Exception as e:
        print(f"Erreur lors de la modification du message : {e}")


def get_email_id():
    """Récupère l'ID du dernier email reçu dans la boîte de réception."""
    try:
        service = authenticate_gmail_api("gmail")
        
        # Lister les messages dans l'ordre décroissant (du plus récent au plus ancien)
        results = service.users().messages().list(userId='me', maxResults=1, labelIds=['INBOX'], q='').execute()
        
        messages = results.get('messages', [])
        if not messages:
            print("Aucun email trouvé.")
            return None

        # Récupérer l'ID du dernier message (le plus récent)
        last_message_id = messages[0]['id']
        print(f"ID du dernier email : {last_message_id}")
        return last_message_id

    except Exception as e:
        print(f"Erreur lors de la récupération du dernier email : {e}")
        return None


def clean_body(body, is_html=False):
    """Nettoyer le contenu d'un email (HTML ou texte brut)"""
    # Vérifier si le contenu est du HTML
    if '<' in body and '>' in body:
        # Traiter comme du HTML
        soup = BeautifulSoup(body, 'html.parser')
        text = soup.get_text()
    else:
        # Traiter comme du texte brut
        text = body

    # Supprimer les caractères invisibles comme \u200c, \u200b, \u200d
    text = re.sub(r'[\u200c\u200b\u200d]', '', text) # Supprime ZWNJ, ZWSP, ZWJ
    
    # Supprimer les lignes vides et espaces multiple
    text = re.sub(r'\n+', '\n', text).strip()  
    text = ' '.join(text.split()) 

    return text



def get_email(number):
    results = authenticate_gmail_api("gmail").users().messages().list(userId='me',labelIds =['INBOX']  , maxResults=number).execute()
    messages = results.get('messages', [])
    return messages if messages else None

 

def tous_les_label():
    
    try:
        service = authenticate_gmail_api("gmail")
        results = service.users().labels().list(userId='me').execute()
        labels = results.get('labels', [])

        if not labels:
            print("Aucun label trouvé.")
            return []
        
        # Filtrer les labels en excluant les labels en majuscules (ex: TRASH, SPAM)
        label_names = [
            label['name']
            for label in labels
            if not label['name'].isupper() or label['name'] in ['TRASH', 'SPAM']
        ]

        return label_names

    except Exception as e:
        print(f"Erreur lors de la récupération des labels : {e}")
        return []



def get_email_details(message_id=None):
    """
    Récupère toutes les informations d'un e-mail à partir de son ID.

    :param service: Un objet service Gmail API authentifié.
    :param user_id: L'ID de l'utilisateur (par défaut 'me' pour l'utilisateur authentifié).
    :param message_id: L'ID du message à récupérer.
    :return: Un dictionnaire contenant les informations de l'e-mail.
    """
    user_id='me'
    if not message_id:
        raise ValueError("L'ID du message est requis.")

    # Récupérer le message
    message = authenticate_gmail_api('gmail').users().messages().get(userId=user_id, id=message_id, format='full').execute()

    # Récupérer les en-têtes
    headers = message['payload']['headers']
    email_data = {
        'Id': message_id,
        'My email': 'lailaarnaud04@gmail.com',
        'From': '',
        'Date': '',
        'Subject': '',
        'Body': ''
    }

    # Extraire les en-têtes pertinents
    for header in headers:
        if header['name'] == 'to':
            email_data['To'] = header['value']
        if header['name'] == 'From':
            email_data['From'] = header['value']
        if header['name'] == 'Date':
            email_data['Date'] = header['value']
        if header['name'] == 'Subject':
            email_data['Subject'] = header['value']

    # Décoder le corps du message (supporte texte brut uniquement ici)
    parts = message['payload'].get('parts', [])
    body = None

     # Décoder le corps du message
    parts = message['payload'].get('parts', [])
    text_body = None
    html_body = None

    if 'data' in message['payload']['body']:
        # Corps du message directement dans le payload
        text_body = base64.urlsafe_b64decode(message['payload']['body']['data']).decode('utf-8')
    else:
        for part in parts:
            mime_type = part.get('mimeType', '')
            if mime_type == 'text/plain' and 'data' in part['body']:
                text_body = base64.urlsafe_b64decode(part['body']['data']).decode('utf-8')
            elif mime_type == 'text/html' and 'data' in part['body']:
                html_body = base64.urlsafe_b64decode(part['body']['data']).decode('utf-8')

    # Prioriser le contenu HTML si disponible, sinon utiliser le texte brut
    if html_body:
        body = clean_body(html_body, is_html=True)
    elif text_body:
        body = clean_body(text_body)
    else:
        body = "Pas de contenu disponible dans cet email."

    email_data['Body'] = body

    return email_data











def main():
    messages = get_email(1)
    
    print(get_email_details(messages[0]['id']))
    #message = get_email_details(messages[0]['id'])
    #print(message)
   
    
# Appeler la fonction main pour exécuter le programme
if __name__ == "__main__":
    main()
