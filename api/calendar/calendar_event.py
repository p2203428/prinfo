from datetime import datetime, timedelta
import sys
import pytz 
sys.path.append('api')
from authenticate import *
"""import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from authenticate import *"""


def create_event(extract):
    """
    Crée un événement dans Google Calendar avec le nom, la date, la durée, et la récurrence spécifiés.
    
    Args:
        service: L'API Google Calendar.
        event_name: Nom de l'événement.
        event_datetime: Date et heure de l'événement au format 'YYYY-MM-DDTHH:MM:SS'.
        event_duration: Durée de l'événement (en minutes M ou heures H).
        recurrence: Liste de règles de récurrence.
        is_all_day: Indique si l'événement est une journée entière.
    
    event_datetime = f"{event_date}T{event_time}:00"

    # Date de début et de fin de l'événement
    start = datetime.datetime.strptime(event_datetime, '%Y-%m-%dT%H:%M:%S').isoformat()
    end = (datetime.datetime.strptime(event_datetime, '%Y-%m-%dT%H:%M:%S') + datetime.timedelta(hours=1)).isoformat()

    
    #création de l'objet de l'événement
    event = {
        'summary': event_name,
        'start': {
            'date': start if is_all_day else None,  # For all-day events
            'dateTime': None if is_all_day else start,  # For timed events
            'timeZone': 'Europe/Paris',
        },
        'end': {
            'date': end if is_all_day else None,  # For all-day events
            'dateTime': None if is_all_day else end,  # For timed events
            'timeZone': 'Europe/Paris',
        }
    }

    # Ajouter les règles de récurrence si elles existent
    if recurrence:
        event['recurrence'] = recurrence
'''
    # Appel à l'API Google Calendar pour insérer l'événement
    #event = authenticate_gmail_api("calendar").events().insert(calendarId='primary', body=event).execute()"""
    event = authenticate_gmail_api("calendar").events().insert(calendarId='primary', body=extract).execute()
    print(f"Événement créé : {event.get('htmlLink')}")




    
def delete_event(event_id):
    """
    Supprime un événement dans Google Calendar par son ID.

    Args:
        service: l'API Google Calendar. 
        event_id: ID de l'événement à supprimer.
    """
    try:
        authenticate_gmail_api("calendar").events().delete(calendarId='primary', eventId=event_id).execute()
        print(f"Événement supprimé.")
    except Exception as e:
        print(f"Erreur lors de la suppression : {e}")
        
        
    
    
def list_events(from_date, to_date, from_time, to_time):
    """
    Liste tous les événements dans le calendrier Google de l'utilisateur.

    Args:
        from_date (str): La date de début au format 'YYYY-MM-DD'.
        to_date (str): La date de fin au format 'YYYY-MM-DD'.
        from_time (str): L'heure de début au format 'HH:MM'.
        to_time (str): L'heure de fin au format 'HH:MM'.
    """
    from_date = datetime.strptime(from_date, '%Y-%m-%d')
    to_date = datetime.strptime(to_date, '%Y-%m-%d')

    current_date = from_date
    while current_date <= to_date:
        # Construct the timeMin and timeMax values using the provided date and time
        time_min = f"{current_date.strftime('%Y-%m-%d')}T{from_time}:00Z"
        time_max = f"{current_date.strftime('%Y-%m-%d')}T{to_time}:00Z"
        
        # Fetch events from Google Calendar API
        events_result = authenticate_gmail_api("calendar").events().list(
            calendarId='primary',
            timeMin=time_min,
            timeMax=time_max,
            singleEvents=True,
            orderBy='startTime'
        ).execute()

        events = events_result.get('items', [])
        print(f"Événements du {current_date.strftime('%Y-%m-%d')} ({from_time} à {to_time}):")

        if not events:
            print('Aucun événement trouvé.')
        else:
            for event in events:
                start = event['start'].get('dateTime', event['start'].get('date'))
                summary = event.get('summary', '(Pas de titre)')
                print(f"{start} - {summary}")

        print("=" * 50)
        current_date += timedelta(days=1)
    




def check_event_at_date_time(start_date_time, end_time):
    """
    Vérifie s'il existe un événement entre une date/heure de début et une heure de fin.

    Args:
        start_date_time (str): La date et heure de début au format 'DD-MM-YYYY HH:MM'.
        end_time (str): L'heure de fin au format 'HH:MM'.

    Returns:
        (bool, str): Un booléen indiquant si un événement est trouvé et une chaîne avec le titre des événements si trouvés.
    """
    try:
        # Convertir la date et l'heure de début en format ISO pour l'API Google Calendar
        local_tz = pytz.timezone('Europe/Paris')  
        event_start_datetime = local_tz.localize(datetime.strptime(start_date_time, '%d-%m-%Y %H:%M'))

        # Construire l'heure de fin en utilisant la même date que le début
        end_time_obj = datetime.strptime(end_time, '%H:%M').time()
        event_end_datetime = datetime.combine(event_start_datetime.date(), end_time_obj)
        event_end_datetime = local_tz.localize(event_end_datetime)

        # Convertir en UTC pour la requête API
        start_time = event_start_datetime.astimezone(pytz.utc).isoformat()
        end_time = event_end_datetime.astimezone(pytz.utc).isoformat()

        # Requête pour récupérer les événements entre start_time et end_time
        events_result = authenticate_gmail_api("calendar").events().list(
            calendarId='primary', 
            timeMin=start_time, 
            timeMax=end_time, 
            singleEvents=True).execute()
        events = events_result.get('items', [])

        if not events:
            return False, "Aucun(s) événement(s) trouvé(s) pour cette plage de temps."
        else:
            # Afficher tous les événements trouvés dans cette plage de temps
            found_events = []
            for event in events:
                start = event['start'].get('dateTime', event['start'].get('date'))
                end = event['end'].get('dateTime', event['end'].get('date'))
                
                # Convertir l'heure de l'événement UTC en heure locale pour l'affichage
                event_start_time = datetime.fromisoformat(start).astimezone(local_tz)
                event_end_time = datetime.fromisoformat(end).astimezone(local_tz)

                # Filtrer uniquement les événements qui se déroulent entièrement dans l'intervalle donné
                if event_start_time >= event_start_datetime and event_end_time <= event_end_datetime:
                    found_events.append(f"- {event['summary']} à {event_start_time.strftime('%H:%M')}-{event_end_time.strftime('%H:%M')}")

            if not found_events:
                return False, "Aucun(s) événement(s) trouvé(s) pour cette plage de temps."
            
            result = f"Événement(s) trouvé(s) le {event_start_datetime.strftime('%d-%m-%Y')} : \n" + '\n'.join(found_events)
            return True, result

    except Exception as e:
        return False, f"Erreur lors de la vérification de l'événement : {e}"

def main():
    # Demande à l'utilisateur d'entrer la date/heure de début et l'heure de fin
    start_date_time = input("Entrez la date et l'heure de début (DD-MM-YYYY HH:MM) : ")
    end_time = input("Entrez l'heure de fin (HH:MM) : ")

    # Vérifier si un événement existe entre ces deux horaires
    found, result = check_event_at_date_time(start_date_time, end_time)
    print(result)

if __name__ == '__main__':
    main()
